# Tests all combinations of feed rates and laser power levels specified below
# Forms a grid of test strips, starting at the top left corner, moving down and to the right
#
# Tommy Vandermolen
# 22 March 2019

import itertools

# Settings ####
# The feed rates to test, in mm/min
feedrates = [1000, 1500, 2000]
# The laser power levels to test, 0-255
powers = [10, 20, 30, 40, 50, 60, 70, 80]

# Starting coordinate (x, y)
start_coord = (20, 180)
# Length of test strip in the y-direction, in mm
test_length = 20
# X distance between subsequent test strips
test_spacing_x = 8
# Y distance between subsequent rows of test strips
test_spacing_y = 8

# Travel speed, for movements with the laser off, in mm/min
travel_speed = 6000

# Create G-Code ####
cmds = []
x, y = start_coord
n = itertools.count(10, 5)

cmds.append(f'N{next(n)} G90 ; absolute positioning')
cmds.append(f'N{next(n)} G21 ; set units to mm')

for f in feedrates:
    x = start_coord[0]
    cmds.append(f'\n(NEW ROW - FEEDRATE: {f} mm/min)')

    for p in powers:
        cmds.append(f'    (POWER: {p})')
        # Move to start of test strip
        cmds.append(f'N{next(n)} G0 X{x} Y{y} F{travel_speed}')
        # Enable laser
        cmds.append(f'N{next(n)} M106 P0 S{p}')
        # Create test strip
        cmds.append(f'N{next(n)} G1 X{x} Y{y - test_length} F{f}')
        # Disable laser
        cmds.append(f'N{next(n)} M107 P0')
        x += test_spacing_x
    y -= test_spacing_y + test_length

# Output ####
gcode = '\n'.join(cmds)
print(gcode)
with open('laser_test.gcode', 'w') as file:
    file.write(gcode)
